package basic.android.incomingcallhawk;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;

import basic.android.incomingcallhawk.Model.PhoneInfo;

public class MyApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();

    }
}
