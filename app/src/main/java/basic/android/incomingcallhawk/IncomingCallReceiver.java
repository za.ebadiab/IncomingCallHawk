package basic.android.incomingcallhawk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;


import java.text.SimpleDateFormat;
import java.util.Date;

import basic.android.incomingcallhawk.Model.PhoneInfo;

public class IncomingCallReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {



        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)|| state.equals(TelephonyManager.CALL_STATE_OFFHOOK) ) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                String callStartTime = sdf.format(new Date());


                PhoneInfo phoneInfo = new PhoneInfo(incomingNumber, callStartTime);

                Hawk.put("phoneInfo", phoneInfo);

              }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

