package basic.android.incomingcallhawk.Model;

public class PhoneInfo {

    private String phoneNumber;
    private String dateTime;

    public PhoneInfo(String phoneNumber, String dateTime) {
        this.phoneNumber = phoneNumber;
        this.dateTime = dateTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
