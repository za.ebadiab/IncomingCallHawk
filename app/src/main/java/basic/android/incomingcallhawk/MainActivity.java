package basic.android.incomingcallhawk;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;

import basic.android.incomingcallhawk.Model.PhoneInfo;

public class MainActivity extends BaseActivity {

    TextView txtDate, txtNumber;
    PhoneInfo phoneInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();

        checkPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHawkData();

    }

    public void bind() {
        txtDate = findViewById(R.id.txtDate);
        txtNumber = findViewById(R.id.txtNumber);
    }

    public void getHawkData() {
        phoneInfo = Hawk.get("phoneInfo", null);

        if (phoneInfo != null) {
            txtDate.setText(phoneInfo.getDateTime());
            txtNumber.setText(phoneInfo.getPhoneNumber());
        }
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        1000);
            }
        }
    }
}


